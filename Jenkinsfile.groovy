@Library('usta-utils-mule') _

properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5'))])

node {
    stage('Checkout sources') {
        checkout scm
    }
    echo "Loading ecosystem..."
    def ecosystem = utils.getEcosystem('usta-mule-ecosystem')


    echo "Creating parameters..."
    def inputProps = []
    inputProps.addAll(utils.getDockerInputParametersList(ecosystem, false))
    utils.generateInputFields(inputProps)

    echo "Running pipeline..."
    runner.startDockerPipeline(params, ecosystem, 'admin')
}