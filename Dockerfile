FROM alpine-java:base
MAINTAINER usta.com
COPY target/*.jar /opt/spring-boot/lib/api-springadmin.jar
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "/opt/spring-boot/lib/api-springadmin.jar", "-Xmx400m", "-Xms100m","-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n", "--spring.config.location=classpath:/application.properties"]
EXPOSE 9000